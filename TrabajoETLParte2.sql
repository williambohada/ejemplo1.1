

--********************* SEGUNDA PARTE ETL *************************************

--1 INVENTARIO 
SELECT  A.DINAMICA AS Producto
        ,'fxodaily' AS TipoOperacion
        ,TO_CHAR(fecha_celebracion_contrato, 'MON/DD/YYYY') AS FechaNegociacion
        ,CONCAT('FXO', A.numero_de_contrato) AS Referencia
        ,A.nombre_contraparte AS Cliente
        ,A.numero_iden_contraparte AS NIT
        ,'INVENTARIO' AS TipoNegocio
        ,A.par_de_moneda_negociado AS Pairs
        ,A.nominal AS Monto1
        ,a.nominal_segunda_moneda as Monto2
        ,' ' AS PaymentComments  
        ,A.PRECIO_EJERCICIO AS TasaNegociacion
        ,A.spotrate AS TasaPago
        ,TO_CHAR(A.FECHA_VENCIMIENTO_CONTRATO,'MON/DD/YYYY') AS FechaPago
        ,CONCAT(CONCAT(A.nombre_usuario,' - '),COALESCE(A.numero_iden_usuario,'')) AS Usuario
        ,A.folder AS Folder


FROM SABANA_OPTIONS A
INNER JOIN RTK_OPTIONS B ON A.fecha_corte = B.fecha_corte AND a.numero_de_contrato = B.FXOPTIONSDEALS_ID
WHERE  
      A.Fecha_Corte >= A.Fecha_celebracion_contrato AND 
      A.Fecha_Corte < A.Fecha_Vencimiento_Contrato  AND
      A.Fecha_Corte = '11/06/21' and  --Fecha_Corte = ‘VALOR QUE ESTE EN EL ARCHIVO DE PARAMETROS’, ej: 02/08/2021
      A.Estado_operacion = 'V';
      
   

-- 2 CUMPLIMIENTO  DELIVERY
SELECT  'OPCIONES' AS Producto
        ,A.compra_o_venta AS TipoOperacion
        ,TO_CHAR(A.fecha_liq_contrato, 'MON/DD/YYYY') AS FechaNegociacion
        ,CONCAT(CONCAT('OP', A.numero_de_contrato),'V') AS Referencia
        ,A.nombre_contraparte AS Cliente
        ,A.numero_iden_contraparte AS NIT
        ,'Cumplimiento delivery' AS TipoNegocio
        ,A.par_de_moneda_negociado AS Pairs
        ,A.nominal AS Monto1
        ,a.nominal_segunda_moneda as Monto2
        ,' ' AS PaymentComments   
        ,A.PRECIO_EJERCICIO AS TasaNegociacion
        ,A.spotrate AS TasaPago
        ,TO_CHAR(A.FECHA_VENCIMIENTO_CONTRATO,'MON/DD/YYYY') AS FechaPago
        ,CONCAT(CONCAT(A.nombre_usuario,' - '),COALESCE(A.numero_iden_usuario,'')) AS Usuario
        ,A.folder AS Folder

FROM SABANA_OPTIONS A
INNER JOIN RTK_OPTIONS B ON A.fecha_corte = B.fecha_corte AND a.numero_de_contrato = B.FXOPTIONSDEALS_ID
WHERE 

      A.FECHA_CORTE = A.fecha_liq_contrato   AND 
      A.Opcion_Ejercida = 'SI'  AND
      A.moneda_liq <> 'COP'  and
      A.Fecha_Corte = '11/06/21' and  -- a.fecha_corte = VALOR DEL PARAMETRO        
      a.FORMA_LIQUIDACION = 'D'  AND 
      A.estado_operacion = 'V';
      
     
--3 CUMPLIMIENTO NO DELIVERY

SELECT  'OPCIONES' AS Producto
        ,A.compra_o_venta AS TipoOperacion
        ,TO_CHAR(A.fecha_liq_contrato, 'MON/DD/YYYY') AS FechaNegociacion
        ,CONCAT(CONCAT('OP', A.numero_de_contrato),'V') AS Referencia
        ,A.nombre_contraparte AS Cliente
        ,A.numero_iden_contraparte AS NIT      
        ,'Cumplimiento No delivery' AS TipoNegocio
        ,A.par_de_moneda_negociado AS Pairs
        ,DECODE(A.COMPRA_O_VENTA,'Compra',0,'Venta',B.CASHSETTLEMENT)AS Monto1
        ,DECODE(A.COMPRA_O_VENTA,'Compra',B.CASHSETTLEMENT,'Venta',0)AS Monto2
        ,' ' AS PaymentComments  
        ,A.PRECIO_EJERCICIO AS TasaNegociacion
        ,A.spotrate AS TasaPago
        ,TO_CHAR(A.FECHA_VENCIMIENTO_CONTRATO,'MON/DD/YYYY') AS FechaPago
        ,CONCAT(CONCAT(A.nombre_usuario,' - '),COALESCE(A.numero_iden_usuario,'')) AS Usuario
        ,A.folder AS Folder

FROM SABANA_OPTIONS A
INNER JOIN RTK_OPTIONS B ON A.fecha_corte = B.fecha_corte AND a.numero_de_contrato = B.FXOPTIONSDEALS_ID
WHERE 

      A.FECHA_CORTE = A.fecha_liq_contrato   AND 
      A.FORMA_LIQUIDACION = 'c'  AND 
      A.Opcion_Ejercida = 'SI'  AND
      A.moneda_liq <> 'COP'  and
      A.Fecha_Corte = '11/06/21' and  -- a.fecha_corte = VALOR DEL PARAMETRO        
      A.estado_operacion = 'V';
      
-------------******************************************************************************************************

--4 CUMPLIMIENTO PAGO PRIMA

SELECT  'OPCIONES' AS Producto
        ,A.compra_o_venta AS TipoOperacion
        ,TO_CHAR(A.fecha_liq_contrato, 'MON/DD/YYYY') AS FechaNegociacion
        ,CONCAT(CONCAT('OP', A.numero_de_contrato),'P') AS Referencia
        ,A.nombre_contraparte AS Cliente
        ,A.numero_iden_contraparte AS NIT
        ,'Cumplimiento PAGO_PRIMA' AS TipoNegocio
        ,A.par_de_moneda_negociado AS Pairs
        ,0 AS Monto1
        ,DECODE(A.CODIGOOFFSHORE,'Si',A.VALOR_PRIMA_MONEDA1,'No',VALOR_PRIMA_PACTADA)AS Monto2
        ,' ' AS PaymentComments   
        ,A.PRECIO_EJERCICIO AS TasaNegociacion
        ,A.spotrate AS TasaPago
        ,TO_CHAR(A.FECHA_VENCIMIENTO_CONTRATO,'MON/DD/YYYY') AS FechaPago
        ,CONCAT(CONCAT(A.nombre_usuario,' - '),COALESCE(A.numero_iden_usuario,'')) AS Usuario
        ,A.folder AS Folder

FROM SABANA_OPTIONS A
INNER JOIN RTK_OPTIONS B ON A.fecha_corte = B.fecha_corte AND a.numero_de_contrato = B.FXOPTIONSDEALS_ID
WHERE 

      A.FECHA_CORTE = A.fecha_liq_contrato and
      a.fecha_corte = A.fecha_pago_prima  and
      a.moneda_liq <> 'COP' and
      A.Fecha_Corte = '11/06/21' and  -- a.fecha_corte = VALOR DEL PARAMETRO  
      a.estado_operacion = 'V';



--*****************  UPDATE *************************************************************************************************
DECLARE
--ejemplo VARCHAR(25);
BEGIN
UPDATE SABANA_OPTIONS SET FECHA_CORTE = '04/02/21'
WHERE DINAMICA = 'FXOSC';
END;  


ALTER TABLE SABANA_OPTIONS
ADD Opcion_Ejercida varchar2(3);


ALTER TABLE SABANA_OPTIONS
ADD moneda_liq varchar2(3);

DECLARE
--ejemplo VARCHAR(25);
BEGIN
UPDATE SABANA_OPTIONS SET opcion_Ejercida = 'NO'
WHERE FECHA_CORTE = '04/02/21';
END;  


DECLARE
--ejemplo VARCHAR(25);
BEGIN
UPDATE SABANA_OPTIONS SET moneda_liq = 'cop'
WHERE FECHA_CORTE = '11/06/21';
END;  

DECLARE
--ejemplo VARCHAR(25);
BEGIN
UPDATE SABANA_OPTIONS SET ESTADO_OPERACION = 'C'
WHERE COMPRA_O_VENTA = 'Compra';
END;   

DECLARE
--ejemplo VARCHAR(25);
BEGIN
UPDATE SABANA_OPTIONS SET FORMA_LIQUIDACION = 'D'
WHERE COMPRA_O_VENTA = 'Compra';
END; 

DECLARE
--ejemplo VARCHAR(25);
BEGIN
UPDATE SABANA_OPTIONS SET FORMA_LIQUIDACION = 'c'
WHERE COMPRA_O_VENTA = 'Venta';
END; 

DECLARE
--ejemplo VARCHAR(25);
BEGIN
UPDATE SABANA_OPTIONS SET FORMA_LIQUIDACION = 'D'
WHERE moneda_compra = 'COP';
END;  


DECLARE
--ejemplo VARCHAR(25);
BEGIN
UPDATE SABANA_OPTIONS SET fecha_liq_contrato = '11/06/21'
WHERE moneda_compra = 'COP';
END;  

DECLARE
--ejemplo VARCHAR(25);
BEGIN
UPDATE rtk_options SET CASHSETTLEMENT = 1
WHERE CASHSETTLEMENT = 0;
END;  